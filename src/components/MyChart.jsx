import React, { useState, useEffect } from 'react'
import { Chart } from 'react-charts'
 
export default function MyChart() {
  const [items, setItems] = useState([]); 
  const [isLoaded, setIsLoaded] = useState(false);
 
  const axes = React.useMemo(
    () => [
      { primary: true, type: 'utc', position: 'bottom' },
      { type: 'linear', position: 'left' }
    ],
    []
  )

  useEffect(() => {
      fetch("http://localhost:5000/sensorReadings")
        .then(res => res.json())
        .then(
            (result) => {
                console.log(result);
                let weatherDataForChart = [{label: 'Series 1', data: parseWeatherData(result, 'pijordan.tx.rr.com')},
                                           {label: 'Series 2', data: parseWeatherData(result, 'jordanoutside')}]
                console.log(weatherDataForChart);
                setItems(weatherDataForChart);
                setIsLoaded(true);
                
            },
            (error) => {
                setIsLoaded(true);
              }
        )
  }, [])

  const parseWeatherData = (weatherData, sensorName) => {
    const sensorData = weatherData.filter(weatherItem => weatherItem.sensor === sensorName)
    const dateAndTempList = sensorData.map(weatherItem => [new Date(setCharAt(weatherItem.timestamp,10,'T')), weatherItem.temperature])
    const sortedDateAndTempList = dateAndTempList.sort(function(a,b){
        // Turn your strings into dates, and then subtract them
        // to get a value that is either negative, positive, or zero.
        return a[0] - b[0];
      });
    return sortedDateAndTempList;
  }

  const setCharAt = (str,index,chr) => {
    if(index > str.length-1) return str;
    return str.substring(0,index) + chr + str.substring(index+1);
}
    
  if(!isLoaded) {
      console.log(isLoaded)
      return (<div>Loading...</div>);
  } else {
    return (
        // A react-chart hyper-responsively and continuously fills the available
        // space of its parent element automatically
        <div
        style={{
            width: '500px',
            height: '300px'
        }}
        >
        <Chart data={items} 
               axes={axes} 
               tooltip
               primaryCursor
               secondaryCursor />
        </div>
    );
  } 
}